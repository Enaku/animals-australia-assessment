const STATES = [
  { value: 'NSW', label: 'NSW'},
  { value: 'NT', label: 'NT'},
  { value: 'QLD', label:' QLD'},
  { value: 'SA', label: 'SA'},
  { value: 'VIC', label:' VIC'},
  { value: 'WA', label: 'WA'},
  { value: 'TAS', label:' TAS'},
  { value: 'non_australian', label: 'Non Australian'}
];

export default STATES;
