export var CONTAINER_STYLE =  {
  maxWidth: '964px',
  margin: '0 auto'
};

export var MOBILE_CONTAINER_STYLE = {
  marginLeft: 30,
  marginRight: 30
}
