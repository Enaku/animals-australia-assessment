const STATE_MPS = [
  {
    "surname": "Aitchison",
    "party": "Labor",
    "electorate": "Maitland",
    "first_name": "Jenny"
  },
  {
    "surname": "Anderson",
    "party": "National",
    "electorate": "Tamworth",
    "first_name": "Kevin"
  },
  {
    "surname": "Aplin",
    "party": "Liberal",
    "electorate": "Albury",
    "first_name": "Greg"
  },
  {
    "surname": "Atalla",
    "party": "Labor",
    "electorate": "Mount Druitt",
    "first_name": "Edmond"
  },
  {
    "surname": "Ayres",
    "party": "Liberal",
    "electorate": "Penrith",
    "first_name": "Stuart"
  },
  {
    "surname": "Baird",
    "party": "Liberal",
    "electorate": "Manly",
    "first_name": "Mike"
  },
  {
    "surname": "Barilaro",
    "party": "National",
    "electorate": "Monaro",
    "first_name": "John"
  },
  {
    "surname": "Barr",
    "party": "Labor",
    "electorate": "Cessnock",
    "first_name": "Clayton"
  },
  {
    "surname": "Berejiklian",
    "party": "Liberal",
    "electorate": "Willoughby",
    "first_name": "Gladys"
  },
  {
    "surname": "Bromhead",
    "party": "National",
    "electorate": "Myall Lakes",
    "first_name": "Stephen"
  },
  {
    "surname": "Brookes",
    "party": "Liberal",
    "electorate": "East Hills",
    "first_name": "Glenn"
  },
  {
    "surname": "Burney",
    "party": "Labor",
    "electorate": "Canterbury",
    "first_name": "Linda"
  },
  {
    "surname": "Car",
    "party": "Labor",
    "electorate": "Londonderry",
    "first_name": "Prue"
  },
  {
    "surname": "Catley",
    "party": "Labor",
    "electorate": "Swansea",
    "first_name": "Yasmin"
  },
  {
    "surname": "Chanthivong",
    "party": "Labor",
    "electorate": "Macquarie Fields",
    "first_name": "Anoulack"
  },
  {
    "surname": "Conolly",
    "party": "Liberal",
    "electorate": "Riverstone",
    "first_name": "Kevin"
  },
  {
    "surname": "Constance",
    "party": "Liberal",
    "electorate": "Bega",
    "first_name": "Andrew"
  },
  {
    "surname": "Coure",
    "party": "Liberal",
    "electorate": "Oatley",
    "first_name": "Mark"
  },
  {
    "surname": "Crakanthorp",
    "party": "Labor",
    "electorate": "Newcastle",
    "first_name": "Tim"
  },
  {
    "surname": "Crouch",
    "party": "Liberal",
    "electorate": "Terrigal",
    "first_name": "Adam"
  },
  {
    "surname": "Daley",
    "party": "Labor",
    "electorate": "Maroubra",
    "first_name": "Michael"
  },
  {
    "surname": "Davies",
    "party": "Liberal",
    "electorate": "Mulgoa",
    "first_name": "Tanya"
  },
  {
    "surname": "Dib",
    "party": "Labor",
    "electorate": "Lakemba",
    "first_name": "Jihad"
  },
  {
    "surname": "Dominello",
    "party": "Liberal",
    "electorate": "Ryde",
    "first_name": "Victor"
  },
  {
    "surname": "Doyle",
    "party": "Labor",
    "electorate": "Blue Mountains",
    "first_name": "Trish"
  },
  {
    "surname": "Elliott",
    "party": "Liberal",
    "electorate": "Baulkham Hills",
    "first_name": "David"
  },
  {
    "surname": "Evans",
    "party": "Liberal",
    "electorate": "Heathcote",
    "first_name": "Lee"
  },
  {
    "surname": "Finn",
    "party": "Labor",
    "electorate": "Granville",
    "first_name": "Julia"
  },
  {
    "surname": "Foley",
    "party": "Labor",
    "electorate": "Auburn",
    "first_name": "Luke"
  },
  {
    "surname": "Fraser",
    "party": "National",
    "electorate": "Coffs Harbour",
    "first_name": "Andrew"
  },
  {
    "surname": "Gee",
    "party": "National",
    "electorate": "Orange",
    "first_name": "Andrew"
  },
  {
    "surname": "George",
    "party": "National",
    "electorate": "Lismore",
    "first_name": "Thomas"
  },
  {
    "surname": "Gibbons",
    "party": "Liberal",
    "electorate": "Holsworthy",
    "first_name": "Melanie"
  },
  {
    "surname": "Goward",
    "party": "Liberal",
    "electorate": "Goulburn",
    "first_name": "Prue"
  },
  {
    "surname": "Grant",
    "party": "National",
    "electorate": "Dubbo",
    "first_name": "Troy"
  },
  {
    "surname": "Greenwich",
    "party": "Independent",
    "electorate": "Sydney",
    "first_name": "Alex"
  },
  {
    "surname": "Gulaptis",
    "party": "National",
    "electorate": "Clarence",
    "first_name": "Christopher"
  },
  {
    "surname": "Hancock",
    "party": "Liberal",
    "electorate": "South Coast",
    "first_name": "Shelley"
  },
  {
    "surname": "Harrison",
    "party": "Labor",
    "electorate": "Charlestown",
    "first_name": "Jodie"
  },
  {
    "surname": "Harris",
    "party": "Labor",
    "electorate": "Wyong",
    "first_name": "David"
  },
  {
    "surname": "Haylen",
    "party": "Labor",
    "electorate": "Summer Hill",
    "first_name": "Jo"
  },
  {
    "surname": "Hay",
    "party": "Labor",
    "electorate": "Wollongong",
    "first_name": "Noreen"
  },
  {
    "surname": "Hazzard",
    "party": "Liberal",
    "electorate": "Wakehurst",
    "first_name": "Brad"
  },
  {
    "surname": "Henskens",
    "party": "Liberal",
    "electorate": "Ku-ring-gai",
    "first_name": "Alister"
  },
  {
    "surname": "Hodgkinson",
    "party": "National",
    "electorate": "Cootamundra",
    "first_name": "Katrina"
  },
  {
    "surname": "Hoenig",
    "party": "Labor",
    "electorate": "Heffron",
    "first_name": "Ron"
  },
  {
    "surname": "Hornery",
    "party": "Labor",
    "electorate": "Wallsend",
    "first_name": "Sonia"
  },
  {
    "surname": "Humphries",
    "party": "National",
    "electorate": "Barwon",
    "first_name": "Kevin"
  },
  {
    "surname": "Johnsen",
    "party": "National",
    "electorate": "Upper Hunter",
    "first_name": "Michael"
  },
  {
    "surname": "Kamper",
    "party": "Labor",
    "electorate": "Rockdale",
    "first_name": "Stephen"
  },
  {
    "surname": "Kean",
    "party": "Liberal",
    "electorate": "Hornsby",
    "first_name": "Matt"
  },
  {
    "surname": "Lalich",
    "party": "Labor",
    "electorate": "Cabramatta",
    "first_name": "Nick"
  },
  {
    "surname": "Lee",
    "party": "Liberal",
    "electorate": "Parramatta",
    "first_name": "Geoff"
  },
  {
    "surname": "Leong",
    "party": "Greens",
    "electorate": "Newtown",
    "first_name": "Jenny"
  },
  {
    "surname": "Lynch",
    "party": "Labor",
    "electorate": "Liverpool",
    "first_name": "Paul"
  },
  {
    "surname": "Maguire",
    "party": "Liberal",
    "electorate": "Wagga Wagga",
    "first_name": "Daryl"
  },
  {
    "surname": "Marshall",
    "party": "National",
    "electorate": "Northern Tablelands",
    "first_name": "Adam"
  },
  {
    "surname": "McDermott",
    "party": "Labor",
    "electorate": "Prospect",
    "first_name": "Hugh"
  },
  {
    "surname": "McKay",
    "party": "Labor",
    "electorate": "Strathfield",
    "first_name": "Jodie"
  },
  {
    "surname": "Mehan",
    "party": "Labor",
    "electorate": "The Entrance",
    "first_name": "David"
  },
  {
    "surname": "Mihailuk",
    "party": "Labor",
    "electorate": "Bankstown",
    "first_name": "Tania"
  },
  {
    "surname": "Minns",
    "party": "Labor",
    "electorate": "Kogarah",
    "first_name": "Christopher"
  },
  {
    "surname": "Notley-Smith",
    "party": "Liberal",
    "electorate": "Coogee",
    "first_name": "Bruce"
  },
  {
    "surname": "O'Dea",
    "party": "Liberal",
    "electorate": "Davidson",
    "first_name": "Jonathan"
  },
  {
    "surname": "Parker",
    "party": "Greens",
    "electorate": "Balmain",
    "first_name": "Jamie"
  },
  {
    "surname": "Park",
    "party": "Labor",
    "electorate": "Keira",
    "first_name": "Ryan"
  },
  {
    "surname": "Patterson",
    "party": "Liberal",
    "electorate": "Camden",
    "first_name": "Christopher"
  },
  {
    "surname": "Pavey",
    "party": "National",
    "electorate": "Oxley",
    "first_name": "Melinda"
  },
  {
    "surname": "Perrottet",
    "party": "Liberal",
    "electorate": "Hawkesbury",
    "first_name": "Dominic"
  },
  {
    "surname": "Petinos",
    "party": "Liberal",
    "electorate": "Miranda",
    "first_name": "Eleni"
  },
  {
    "surname": "Piccoli",
    "party": "National",
    "electorate": "Murray",
    "first_name": "Adrian"
  },
  {
    "surname": "Piper",
    "party": "Independent",
    "electorate": "Lake Macquarie",
    "first_name": "Greg"
  },
  {
    "surname": "Provest",
    "party": "National",
    "electorate": "Tweed",
    "first_name": "Geoff"
  },
  {
    "surname": "Robertson",
    "party": "Labor",
    "electorate": "Blacktown",
    "first_name": "John"
  },
  {
    "surname": "Roberts",
    "party": "Liberal",
    "electorate": "Lane Cove",
    "first_name": "Anthony"
  },
  {
    "surname": "Rowell",
    "party": "Liberal",
    "electorate": "Wollondilly",
    "first_name": "Jai"
  },
  {
    "surname": "Sidoti",
    "party": "Liberal",
    "electorate": "Drummoyne",
    "first_name": "John"
  },
  {
    "surname": "Skinner",
    "party": "Liberal",
    "electorate": "North Shore",
    "first_name": "Jillian"
  },
  {
    "surname": "Smith",
    "party": "Labor",
    "electorate": "Gosford",
    "first_name": "Kathy"
  },
  {
    "surname": "Smith",
    "party": "Greens",
    "electorate": "Ballina",
    "first_name": "Tamara"
  },
  {
    "surname": "Speakman",
    "party": "Liberal",
    "electorate": "Cronulla",
    "first_name": "Mark"
  },
  {
    "surname": "Stokes",
    "party": "Liberal",
    "electorate": "Pittwater",
    "first_name": "Rob"
  },
  {
    "surname": "Taylor",
    "party": "Liberal",
    "electorate": "Seven Hills",
    "first_name": "Mark"
  },
  {
    "surname": "Toole",
    "party": "National",
    "electorate": "Bathurst",
    "first_name": "Paul"
  },
  {
    "surname": "Tudehope",
    "party": "Liberal",
    "electorate": "Epping",
    "first_name": "Damien"
  },
  {
    "surname": "Upton",
    "party": "Liberal",
    "electorate": "Vaucluse",
    "first_name": "Gabrielle"
  },
  {
    "surname": "Ward",
    "party": "Liberal",
    "electorate": "Kiama",
    "first_name": "Garth"
  },
  {
    "surname": "Warren",
    "party": "Labor",
    "electorate": "Campbelltown",
    "first_name": "Greg"
  },
  {
    "surname": "Washington",
    "party": "Labor",
    "electorate": "Port Stephens",
    "first_name": "Kate"
  },
  {
    "surname": "Watson",
    "party": "Labor",
    "electorate": "Shellharbour",
    "first_name": "Anna"
  },
  {
    "surname": "Williams",
    "party": "Liberal",
    "electorate": "Castle Hill",
    "first_name": "Leslia"
  },
  {
    "surname": "Williams",
    "party": "National",
    "electorate": "Port Macquarie",
    "first_name": "Ray"
  },
  {
    "surname": "Zangari",
    "party": "Labor",
    "electorate": "Fairfield",
    "first_name": "Guy"
  }
];

export default STATE_MPS;
