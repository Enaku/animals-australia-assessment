import React from 'react';
import UpdateAction from './update-action';
import { ANIMALS_AUSTRALIA_LIGHT_ORANGE, TEXT_COLOR, ANIMALS_AUSTRALIA_LIGHT_GREY } from '../constants/colours';

function updateActionStyle(y, opacity) { 
  return {
    transformOrigin: '50% 50% 0px',
    position: 'absolute',
    height: '44px',
    transform: `translate3d(0, ${y}px, 0)`,
    borderBottom: `1px solid ${ANIMALS_AUSTRALIA_LIGHT_GREY}`,
    opacity
  };
}

const numberStyle = {
  color: ANIMALS_AUSTRALIA_LIGHT_ORANGE,
  fontWeight: 'bold',
  fontSize: '0.75em'
}

const containerStyle = {
  display: 'inline-block'
};

const nameAndLocationStyle = {
  display: 'inline-block',
  color: TEXT_COLOR,
  fontSize: '0.75em',
  marginLeft: '16px',
  marginRight: '42px'
}

const dateStyle = {
  color: TEXT_COLOR,
  fontSize: '0.75em',
  marginTop: '1.1em',
  display: 'inline-block'
};

export default function({number, name, location, date, y, opacity}) {
  return (
    <div style={updateActionStyle(y, opacity)}>
      <div style={containerStyle}>
        <span style={numberStyle}>#{number.toLocaleString()}</span>
      </div>
      <div style={nameAndLocationStyle}>
        <p style={{marginTop: '0', marginBottom: '0'}}><strong> {name},</strong></p>
        <p style={{marginTop: '0'}}><span> {location}</span></p>
      </div>
      <div style={containerStyle}>
        <span style={dateStyle}>{date}</span>
      </div>
    </div>
  );
};
