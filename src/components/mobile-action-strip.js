import React from 'react';
import STATES from '../constants/states';
import STATE_MPS from '../constants/state_mps';
import { CONTAINER_STYLE, MOBILE_CONTAINER_STYLE } from '../constants/styles';
import { ANIMALS_AUSTRALIA_LIGHT_ORANGE, TEXT_COLOUR } from '../constants/colours';
import ActionAlert from './action-alert';
import Counter from './counter';
import Select from 'react-select';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

const ELECTORATE_OPTIONS = STATE_MPS.map(s => { 
    return { value: s.electorate, label: s.electorate }
  })
  .sort((a, b) => {
    if (a.label < b.label) {
      return -1;
    } else if (a.label > b.label) {
      return 1;
    } else {
      return 0;
    }
  });

export default function(props) {
  const unsureTextStyle = {
    color: '#fff',
    fontFamily: 'Arial, Helvetica',
    fontSize: '1em',
    verticalAlign: 'top',
    fontStyle: 'italic',
    textAlign: 'center',
    textDecoration: 'underline'
  };

  const buttonStyle = {
    cursor: 'pointer',
    fontFamily: 'Avenir Next, Helvetica, Sans',
    fontSize: '1.20em',
    fontWeight: 500,
    letterSpacing: '-0.5',
    color: '#fff',
    textAlign: 'center',
    backgroundRepeat: 'repeat-x',
    width: '100%',
    height: '43px',
    paddingTop: '8px',
    borderRadius: '4px',
    backgroundImage: `url('img/button_background.png')`,
    margin: '0 auto',
    marginTop: '20px',
  };

  const imageStyle = {
    width: '100%'
  };

  const headerStyle = {
    fontFamily: 'Avenir, Helvetica, Sans',
    fontWeight: '900',
    letterSpacing: '-0.5',
    fontSize: '1.3em',
    display: 'block',
    marginTop: '0.1em',
    marginLeft: '0.5em',
    marginBottom: '0.5em',
  };

  const subjectInputStyle = {
    color: TEXT_COLOUR,
    padding: 0,
    paddingLeft: '0.5em',
    fontFamily: 'Avenir, Helvetica, Sans',
    borderRadius: '4px',
    height: '31px',
    width: window.innerWidth - 60 - 15,
    marginBottom: '13px',
    border: 'solid 1px #c6cdcb',
    display: 'block',
  };

  const securityCodeInputStyle = {
    color: TEXT_COLOUR,
    padding: 0,
    paddingLeft: '0.5em',
    fontFamily: 'Avenir, Helvetica, Sans',
    borderRadius: '4px',
    borderRadius: '4px',
    height: '33px',
    width: window.innerWidth - 60 - 15 - 108,
    marginBottom: '13px',
    border: 'solid 1px #c6cdcb',
    display: 'inline-block',
  };

  const messageStyle = {
    color: TEXT_COLOUR,
    lineHeight: 'normal',
    width: window.innerWidth - 60 - 15,
    fontFamily: 'Avenir, Helvetica, Sans',
    borderRadius: '4px',
    height: '150px',
    border: 'solid 1px #c6cdcb',
    display: 'block',
    color: TEXT_COLOUR,
    paddingLeft: '0.5em',
    paddingTop: '10px',
    marginBottom: '15px',
  };

  const bulletStyle = {
    margin: '0',
    fontFamily: 'Avenir, Helvetica, Sans',
    fontSize: '1.1em',
    WebkitPaddingStart: '20px',
    marginTop: '5px',
    marginBottom: '20px'
  };

  const showTakingAction = props.showTakingAction;

  const mobileActionStripStyle = {
    cursor: showTakingAction ? 'auto' : 'pointer',
    position: showTakingAction ? 'relative' : 'fixed',
    minHeight: showTakingAction ? window.innerHeight - 80 : 110,
    width: '100%',
    bottom: 0,
    color: '#fff',
    paddingTop: '5px', 
    backgroundColor: ANIMALS_AUSTRALIA_LIGHT_ORANGE,
    overflowX: 'hidden'
  };

  const takingActionTextStyle = {
    fontSize: '1.9em',
    textShadow: '0 0 20px #FEB364, 0 0 0.2em #fff',
    fontFamily: 'Avenir Next',
    fontWeight: '400',
    letterSpacing: '-0.5', 
    textAlign: 'center',    
  };

  const innerContainerStyle = {
    marginLeft: '30px',
    marginRight: '30px',
    display: 'block',
    position: 'relative',
    backgroundColor: ANIMALS_AUSTRALIA_LIGHT_ORANGE,
    paddingBottom: '30px',
  };

  const stageContainerStyle = {
    textAlign: 'center',
    marginBottom: '25px'
  };

  const labelStyle = {
    ...unsureTextStyle,
    textAlign: 'left',
    display: 'inline-block',
    width: '255px',
    verticalAlign: 'top',
    marginLeft: '13px',
    marginBottom: '20px'
  };

  const linkStyle = {
    color: '#fff',
    fontWeight: 'bold'
  };
  
  const crossStyle = {
    pointer: 'cursor', 
    float: 'right', 
    display: 'block',
    marginRight: 15,
  }

  function Cross() {
    return(
      <div style={{width: '100%', height:'25px', marginTop: '15px'}}>
        <img onClick={props.crossHandler} style={crossStyle} src="img/cross.png" height={25} width={25} />
      </div>
    );
  }

  function renderInitialStrip(x) {
    return (
      <div onClick={props.takeActionHandler} style={{display: 'flex', flex: 1,  minHeight: 100}}>
        <div style={{display: 'flex', flex: 1, flexDirection: props.tablet ? 'row': 'column', justifyContent: 'center', alignItems: 'center',}}>
          <h1 style={{...takingActionTextStyle, fontSize: '2.1em', margin: 0}}>TAKE ACTION</h1>
          <div style={{ paddingLeft: props.tablet ? 10 : 0}}>
            <Counter mobile max={props.max} current={props.current} />
          </div>
        </div>
        <div style={{display: 'flex', flex: '0 0 100px', justifyContent: 'center', alignItems: 'center' }}>
          <img style={{height: '80'}} src="img/mobile_action_strip_button.png" />
        </div>
      </div>
    );
  }

  function renderApology(x) {
    return(
      <div>
        <div style={{display: 'block', position: 'relative', width: '100%', paddingTop: '30px'}}>
          <p style={headerStyle}>Thank you for wanting to take action for the victims of greyhound racing.</p>
          <br />
          <p style={headerStyle}>This action is for NSW residents only, but you can make your voice heard by <a style={linkStyle} href="http://www.animalsaustralia.org/take_action/phone-your-mp-greyhound-racing/">calling your state MP.</a></p>
        </div>
      </div>
    );
  }

  function renderStepOne(x) {
    return(
      <div>
        <div style={innerContainerStyle}>
          <h1 style={takingActionTextStyle}>
            TAKE ACTION
          </h1>
          <div style={{margin: '0 auto', width: '100%'}}>
            <div style={{display: 'table', textAlign: 'center', margin: '0 auto', marginBottom: '20px', }}>
              <img height={32} width={32} style={{verticalAlign: 'middle', display: 'table-cell'}} src="img/step_one.png" />
              <div style={{verticalAlign: 'middle', display: 'table-cell'}}>
                <span style={headerStyle}>Locate your state MP:</span>
              </div>
            </div>
            <Select
              className='mobile'
              clearable={false}
              placeholder={'State: '}
              value={props.state}
              onChange={props.stateHandler}
              name="state"
              valueRenderer={(val) => <span>State: <span style={{color: TEXT_COLOUR}} >{val.value}</span></span>}
              options={STATES} />
            <Select
              className='mobile'
              clearable={false}
              name="state"
              placeholder={'Electorate: '}
              value={props.electorate}
              onChange={props.electorateHandler}
              valueRenderer={(val) => <span>Electorate: <span style={{color: TEXT_COLOUR}} >{val.value}</span></span>}
              options={ELECTORATE_OPTIONS} />
            <div style={{textAlign: 'center', marginTop: '40px', marginBottom:'60px'}}>
              <a href="http://streetlist.elections.nsw.gov.au/" style={unsureTextStyle}>Unsure of your electorate?</a>
            </div>
            <div onClick={props.nextStepHandler} style={buttonStyle}>NEXT STEP &raquo;</div>
          </div>
        </div>
      </div>
    );
  }

  function renderStepTwo(x) {
    return(
      <ReactCSSTransitionGroup transitionName={'step-two'} transitionEnterTimeout={400} transitionLeaveTimeout={400} transitionAppear transitionAppearTimeout={400}>
        <div style={innerContainerStyle}>
          <h1 style={takingActionTextStyle}>TAKE ACTION</h1>
          <div style={stageContainerStyle}>
            <div style={{display: 'table', textAlign: 'center', margin: '0 auto', marginBottom: '20px', }}>
              <img height={32} width={32} style={{verticalAlign: 'middle', display: 'table-cell'}} src="img/step_two.png" />
              <div style={{verticalAlign: 'middle', display: 'table-cell'}}>
                  <span style={headerStyle}>Compose your message:</span>
              </div>
            </div>
            <div style={{width: '100%', paddingRight: '1em'}}>
              <input className="mobile" style={subjectInputStyle} placeholder="Subject:" />
            </div>
            <div style={{width: '100%', marginRight: 15}}>
              <textarea 
                key={444}
                onChange={props.messageHandler}
                style={messageStyle}
                value={props.message} />
            </div>

            <div style={{textAlign: 'left', fontFamily: 'Avenir Next', fontSize: '1.1em'}}>Sincerely, your name</div>
            <div onClick={props.nextStepHandler} style={buttonStyle}>NEXT STEP &raquo;</div>
          </div>
        </div>
      </ReactCSSTransitionGroup>
    );
  }

  function renderStepThree(x) {
   return(
      <ReactCSSTransitionGroup transitionName={'step-two'} transitionEnterTimeout={400} transitionLeaveTimeout={400} transitionAppear transitionAppearTimeout={400}>
        <div style={innerContainerStyle}>
          <h1 style={takingActionTextStyle}>TAKE ACTION</h1>
          <div style={{margin: '0 auto', width: '100%'}}>
            <div style={{display: 'table', textAlign: 'center', margin: '0 auto', marginBottom: '20px', }}>
              <img height={32} width={32} style={{verticalAlign: 'middle', display: 'table-cell'}} src="img/step_three.png" />
              <div style={{verticalAlign: 'middle', display: 'table-cell'}}>
                <span style={headerStyle}>We will send this email to:</span>
              </div>
            </div>
            <ul style={bulletStyle}>
              <li>Your State MP &amp; the NSW racing minister</li>
            </ul>
            <input className="mobile" style={subjectInputStyle} placeholder="Your first name:" />
            <input className="mobile" style={subjectInputStyle} placeholder="Your last name:" />
            <input className="mobile" type="email" style={subjectInputStyle} placeholder="Your email:" />
            <input className="mobile" style={securityCodeInputStyle} placeholder="Security Code:" />
            <img style={{verticalAlign: 'top'}} src="img/security_code.png" />

            <br/>

            <input name="keep_me_updated" type="checkbox"/>
            <label htmlFor="keep_me_updated" style={labelStyle}>Keep me updated with this & other Animals Australia campaigns</label>

            <div onClick={props.nextStepHandler} style={buttonStyle}>SUBMIT LETTER &raquo;</div>
          </div>
        </div>
      </ReactCSSTransitionGroup>
    );
  }

  function renderThankyou(x) {
    return(
      <ReactCSSTransitionGroup transitionName={'step-two'} transitionEnterTimeout={400} transitionLeaveTimeout={400} transitionAppear transitionAppearTimeout={400}>
        <div style={{height: window.innerHeight - 80}}>
          <div style={{display: 'block', position: 'relative', width: '100%', paddingTop: '30px'}}>
            <h1 style={takingActionTextStyle}>THANK YOU</h1>
            <p style={headerStyle}>Thanks for taking action to help the victims of Greyhound Racing!</p>
          </div>
        </div>
      </ReactCSSTransitionGroup>
    );
  }

  const step = props.step;

  var Element;
  var transitionName = '';
  if (!props.showTakingAction) {
    transitionName = 'step-two';
    Element = renderInitialStrip;
  } else if (props.mp === null) {
    Element = renderApology;
  } else if (props.step === 0) {
    transitionName = 'step-one';
    Element = renderStepOne;
  } else if (props.step === 1) {
    Element = renderStepTwo;
  } else if (props.step === 2) {
    Element = renderStepThree;
  } else if (props.step === 3) {
    Element = renderThankyou;
  }

  return (
    <div style={mobileActionStripStyle}>
      { props.showTakingAction && <Cross /> }
      <ReactCSSTransitionGroup key={props.step ? props.step : transitionName} transitionName={transitionName} transitionEnterTimeout={400} transitionLeaveTimeout={400} transitionAppear transitionAppearTimeout={400}>
        { Element() }
      </ReactCSSTransitionGroup>
    </div>
  );
};
