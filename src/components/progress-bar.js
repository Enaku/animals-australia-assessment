import React from 'react';
import { ANIMALS_AUSTRALIA_LIGHT_ORANGE } from '../constants/colours';
import { Motion, spring } from 'react-motion';

function progressBarStyle(max, current) {
  return {
    backgroundImage: 'url(\'img/progress_bar_background.png\')',
    width: current / max * 100 + '%',
    height: '12px',
  };
}

const barStyle = {
  padding: '3px',
  backgroundColor: '#dddddd',
  width: '281px',
  marginBottom: '30px'
};


export default function({max, current}) {
  return (
    <div style={ barStyle }>
      <Motion defaultStyle={{x: 0}} style={{x: spring(current, [300, 50])}}>
      { value => <div style={ progressBarStyle(max, value.x) } className='progress-bar' /> }
      </Motion>
    </div>
  );
};
