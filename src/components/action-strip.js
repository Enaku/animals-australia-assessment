// @flow
import React from 'react';
import { CONTAINER_STYLE } from '../constants/styles';
import STATES from '../constants/states';
import STATE_MPS from '../constants/state_mps';
import Select from 'react-select';
import { Motion, spring } from 'react-motion';

const ELECTORATE_OPTIONS = STATE_MPS.map(s => { 
    return { value: s.electorate, label: s.electorate }
  })
  .sort((a, b) => {
    if (a.label < b.label) {
      return -1;
    } else if (a.label > b.label) {
      return 1;
    } else {
      return 0;
    }
  });

const actionStripLeftStyle = {
  fontFamily: 'Avenir Next',
  fontWeight: 'bold',
  width: '190px',
  height: '90px',
  display: 'inline-block',
  paddingTop: '15px',
  verticalAlign: 'top',
  paddingTop: '15px'
};

const actionStripRightStyle = {
  height: '90px',
  display: 'inline-block',
  verticalAlign: 'top',
};

const actionStripRightInnerStyle = {
  paddingTop: '28px',
  verticalAlign: 'top',
  paddingLeft: '31px'
}

const actionStripContainerStyle = {
  backgroundImage: 'url(\'img/action_strip_background.png\')',
  backgroundPosition: 'center',
  color: '#fff',
  height: '90px',
  zIndex: 500,
  verticalAlign: 'top'
}

const actionTextStyle = {
  display: 'inline-block',
  paddingLeft: '6px',
  paddingTop: '6px',
  verticalAlign: 'top',
  fontFamily: 'Avenir',
  fontWeight: 900
}

const unsureTextStyle = {
  color: '#fff',
  fontFamily: 'Arial, Helvetica',
  fontSize: '13px',
  display: 'inline-block',
  paddingTop: '10px',
  marginLeft: '20px',
  verticalAlign: 'top',
  fontStyle: 'italic',
  textDecoration: 'underline'
};

function containerStyle(value: number): object {
  return {
    transform: `translateX(${value}%)`,
    maxWidth: '964px',
    margin: '0 auto'
  };
}

export default function({state, stateHandler, electorateHandler, electorate}) { 
  const electorateOptions = state === 'NSW' ? ELECTORATE_OPTIONS : [];
  return (
    <div style={actionStripContainerStyle}>
      <Motion defaultStyle={{x: -100}} style={{x: spring(0, [150, 17])}}>
      { value => 
        <div style={containerStyle(value.x)}>
          <div style={actionStripLeftStyle}>
            <h3>TAKE ACTION:</h3>
          </div>
          <div style={actionStripRightStyle}>
            <div style={actionStripRightInnerStyle}>
              <img style={actionTextStyle} src="img/step_one.png" width={24} height={24} />
              <span style={actionTextStyle}>Locate your State MP:</span>
              <Select
                clearable={false}
                name="state"
                value={state}
                options={STATES}
                onChange={stateHandler} />
              <Select
                clearable={false}
                name="state"
                value={electorate}
                options={electorateOptions}
                onChange={electorateHandler} />
              <a href="http://streetlist.elections.nsw.gov.au/" style={unsureTextStyle}>Unsure of your electorate?</a>
            </div>
          </div>
        </div>
      }
      </Motion>
    </div>
  );
};
