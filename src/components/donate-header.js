import React from 'react';
import { ANIMALS_AUSTRALIA_GREY } from '../constants/colours';

function donateHeaderStyle(mobile) {
  return {
    color: ANIMALS_AUSTRALIA_GREY,
    fontSize: '1.3em',
    fontFamily: 'Avenir, Helvetica, Sans',
    fontWeight: 400,
    textAlign: mobile ? 'left' : 'center'
  };
}

export default function({mobile}) {
  return <h3 style={donateHeaderStyle(mobile)}>DONATE TO SUPPORT ANIMALS AUSTRALIA’S WORK:</h3>
};
