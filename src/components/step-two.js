import React from 'react';

const rightContainerStyle = {
  width: '282px',
  display: 'inline-block'
};

const subheaderStyle = {
  display: 'inline-block',
  verticalAlign: 'top',
  paddingLeft: '5px',
  paddingTop: '2px',
  fontFamily: 'Avenir',
  fontWeight: 900
};

const bulletStyle = {
  margin: '0',
  fontFamily: 'Avenir, Helvetica, Sans',
  fontSize: '13px',
  WebkitPaddingStart: '20px',
  marginTop: '5px',
  marginBottom: '40px'
};

const inputStyle = {
  fontFamily: 'Avenir, Helvetica, Sans',
  borderRadius: '4px',
  height: '31px',
  width: '264px',
  paddingLeft: '15px',
  marginBottom: '13px',
  border: 'solid 1px #c6cdcb',
  display: 'block'
};

const securityCodeInputStyle = {
  fontFamily: 'Avenir, Helvetica, Sans',
  borderRadiusTopLeft: '4px',
  borderRadiusBottomLeft: '4px',
  height: '31px',
  width: '156px',
  paddingLeft: '15px',
  marginBottom: '13px',
  border: 'solid 1px #c6cdcb'
};

const labelStyle = {
  display: 'inline-block',
  width: '255px',
  verticalAlign: 'top',
  fontSize: '0.6em',
  marginLeft: '13px',
  fontFamily: 'Arial, Helvetica, Sans',
  fontStyle: 'italic',
  marginBottom: '20px'
}

const buttonStyle = {
  fontFamily: 'Avenir Next, Helvetica, Sans',
  fontSize: '1.05em',
  letterSpacing: '-0.5',
  color: '#fff',
  paddingTop: 9,
  textAlign: 'center',
  backgroundRepeat: 'no-repeat',
  width: '279px',
  height: '42px',
  backgroundImage: 'url(\'img/submit_letter_background.png\')'
};



export default function({firstNameHandler, lastNameHandler, submitLetterHandler}) {
  return (
    <div style={rightContainerStyle}>
      <div style={{display: "block"}}>
        <img src="img/step_three.png" width={24} height={24}  />
        <span style={subheaderStyle}>We will send this email to:</span>
      </div>
      <ul style={bulletStyle}>
        <li>Your State MP &amp; the NSW racing minister</li>
      </ul>
      <input onChange={firstNameHandler} style={inputStyle} placeholder="Your first name:" />
      <input onChange={lastNameHandler} style={inputStyle} placeholder="Your last name:" />
      <input style={inputStyle} placeholder="Your email:" />
      <input style={securityCodeInputStyle} placeholder="Security Code:" />
      <img style={{verticalAlign: 'top'}} src="img/security_code.png" />
      <br/>

      <input name="keep_me_updated" type="checkbox"/>
      <label htmlFor="keep_me_updated" style={labelStyle}>Keep me updated with this & other Animals Australia campaigns</label>

      <div onClick={submitLetterHandler}  style={buttonStyle}>SUBMIT LETTER &raquo;</div>
    </div>
  );
};
