import React from 'react';
import Counter from './counter';
import ProgressBar from './progress-bar';
import UpdateActionList from './update-action-list';

export default function({current, max, updateActions}) {
  return (
    <div style={{width: '284px', display: 'inline-block', verticalAlign: 'top'}}>
      <Counter current={current} max={max} />
      <ProgressBar current={current} max={max} />
      <UpdateActionList updateActions={updateActions} />
    </div>
  );
};
