import React from 'react';
import Intro from './intro.js';

export default function({mobile}) {
  return <Intro mobile={mobile}>Your financial support is needed to support Animals Australia’s ongoing work to protect the most vulnerable and abused animals in our society. Our campaigns are funded entirely by public donations. Please give generously to help put an end to animal cruelty.</Intro>;
};
