import React from 'react';

const leftContainerStyle = {
  width: '573px',
  display: 'inline-block',
  marginRight: '107px'
};

const subheaderStyle = {
  display: 'inline-block',
  verticalAlign: 'top',
  paddingLeft: '5px',
  paddingTop: '2px',
  fontFamily: 'Avenir',
  fontWeight: 900
};

const subjectInputStyle = {
  fontFamily: 'Avenir, Helvetica, Sans',
  borderRadius: '4px',
  height: '31px',
  width: '347px',
  paddingLeft: '15px',
  marginBottom: '13px',
  border: 'solid 1px #c6cdcb',
  display: 'block'
};

const signOffStyle = {
  fontFamily: 'Avenir, Sans',
};

function messageStyle(showTextAreaBackground) {
  return {
    lineHeight: 'normal',
    fontFamily: 'Avenir, Helvetica, Sans',
    borderRadius: '4px',
    width: '100%',
    height: '240px',
    border: 'solid 1px #c6cdcb',
    display: 'block',
    paddingLeft: '15px',
    paddingTop: '10px',
    backgroundRepeat: 'no-repeat',
    backgroundAttachment: 'scroll',
    backgroundImage: showTextAreaBackground ? 'url(\'img/text_input_background.png\')' : ''
  };
}

export default function({message, showTextAreaBackground, messageHandler, textAreaHandler, firstName, lastName}) {
  return(
    <div key={5555} style={leftContainerStyle}>
      <div style={{display: "block", marginBottom: '15px'}}>
        <img src="img/step_two.png"width={24} height={24}  />
        <span style={subheaderStyle}>Compose your message below:</span>
      </div>
      <input style={subjectInputStyle} placeholder="Subject:" />
      <textarea 
        key={444}
        onClick={textAreaHandler}
        onChange={messageHandler}
        value={message}
        style={messageStyle(showTextAreaBackground)} />
      <br/>

      <span style={signOffStyle}>Sincerely, {firstName} {lastName}</span>
    </div>
  );
};
