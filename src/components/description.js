import React from 'react';

export default function({children}) {
  return <p style={{fontSize: '1.07em', fontFamily: 'Arial, Helvetica, Sans', letterSpacing: '-0.5'}}>{children}</p>
};
