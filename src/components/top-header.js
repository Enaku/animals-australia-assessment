import React from 'react';
import { CONTAINER_STYLE, MOBILE_CONTAINER_STYLE } from '../constants/styles';
import ActionAlert from './action-alert';


export default function({mobile}) {
  const desktopContainerStyle = {
    maxWidth: '964px',
    margin: '0 auto',
    height: 88
  };

  const imageStyle = {
    height: '64px',
    marginTop: '12px',
    marginBottom: '12px'
  };
  return (
    <div style={{backgroundColor: '#fff'}}>
      <div style={mobile ? MOBILE_CONTAINER_STYLE : desktopContainerStyle}>
        <img style={imageStyle} src="img/animals_australia_logo.png"></img>
        { mobile ? <ActionAlert mobile={mobile} />  : '' }
      </div>
    </div>
  )
};
