import React from 'react';
import { CONTAINER_STYLE } from '../constants/styles';

const headingStyle = {
  fontFamily: 'Avenir, Helvetica, Sans',
  fontWeight: '900px',
  letterSpacing: '-0.5',
  marginTop: '-2px'
};

const introTextStyle = {
  fontFamily: 'Avenir',
  fontWeight: 300,
};

const memberStyle = {
  fontFamily: 'Avenir',
  fontWeight: 300,
  fontSize: '1.5em'
};

const partyStyle = {
  fontFamily: 'Avenir',
  fontWeight: 900,
  textTransform: 'uppercase'
};

const hrStyle = {
  height: '2px',
  color: '#b7b7b7',
  backgroundColor: '#b7b7b7',
  fontSize: 0,
  border: 'none',
  marginTop: '20px',
  marginBottom: '20px'
};

export default function({first_name, surname, party, electorate}) {
  return (
    <div style={CONTAINER_STYLE}>
      <span style={introTextStyle}>Your state member is:</span> <span style={memberStyle}>{first_name} {surname},</span> <span style={partyStyle}>{party} MEMBER FOR {electorate}</span>
      <hr style={hrStyle}/>
    </div>
  );
};
