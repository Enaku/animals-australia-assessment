import React from 'react';
import Intro from './intro';
import SocialButtons from './social-buttons';
import Update from './update';
import Description from './description';
import DonateHeader from './donate-header';
import DonateText from './donate-text';
import DonateCTA from './donate-cta';

function mainBodyStyle(mobile) {
  return {
    width: mobile ? '100%' : '560px',
    display: 'inline-block',
    marginRight: '120px',
    marginBottom: '70px',
    marginTop: '59px'
  };
}

export default function({mobile}) {
  return (
    <div style={mainBodyStyle(mobile)}>
      <Intro mobile={mobile}>
        Despite evidence of dogs killed & buried on a NSW racing property — and allegations of shocking cruelty — an internal inquiry decided NOT to investigate further. Demand an end to this abusive industry!
      </Intro>

      { mobile ? '' : <SocialButtons types={['twitter', 'facebook']} /> }

      <Update date="13 July 2015">Bones found & independent inquiry commissioned</Update>

      <Description>
        Rumours about dogs being shot in the head and dropped in a pit for $50 at the Keinbah Trial Track were so persistent that the current owners of the track asked Greyhound Racing NSW (GRNSW) to excavate. Instead, GRNSW held an internal inquiry, and even though witnesses admitted up to 18 dogs were buried at Keinbah — among other awful allegations — the racing body has refused to take further action.
      </Description>

      <Description>
        The last time a NSW greyhound industry inquiry opted to do nothing, it was in the face of submissions alleging the use of live animals on lures. That was 2013. Two years later, our live baiting investigation rocked the industry to its core — and the GRNSW Board was told by the Racing Minister to resign or be sacked.
      </Description>

      { mobile ? 
        <Description>
          Thankfully a NSW government inquiry with the power to shut this racket down for good is ongoing. So now is a key time to make sure that your MP knows the NSW community wants greyhound racing gone.
        </Description> : '' }

      { mobile ? 
        <Description>
          Please take 1 minute to send a message on behalf of dogs to your state MP and the NSW Racing Minister — bringing their attention to media reports and urging them to END greyhound racing.
        </Description> : '' }

      { mobile ? <SocialButtons mobile types={['twitter', 'facebook']}/> : null }

      <DonateHeader mobile={mobile} />
      <DonateText mobile={mobile} />
      <DonateCTA/>
    </div>
  );
};
