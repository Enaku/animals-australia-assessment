import React from 'react';
import TopHeader from './top-header';
import TopBanner from './top-banner';
import ActionStrip from './action-strip';
import ScrollArea from './scroll-area';
import MainBody from './main-body';
import Counter from './counter';
import Status from './status';
import MobileActionStrip from './mobile-action-strip';
import TakingAction from './taking-action';
import STATE_MPS from '../constants/state_mps';
import Sticky from 'react-sticky';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

const INITIAL_UPDATE_ACTIONS = [
  { number: 2867, name: 'Maggie Stymans', location: 'NSW 4147, Australia', date: '30 Sept 2014' },
  { number: 2866, name: 'Maggie Stymans', location: 'NSW 4147, Australia', date: '30 Sept 2014' },
  { number: 2865, name: 'Maggie Stymans', location: 'NSW 4147, Australia', date: '30 Sept 2014' },
  { number: 2864, name: 'Maggie Stymans', location: 'NSW 4147, Australia', date: '30 Sept 2014' },
  { number: 2863, name: 'Maggie Stymans', location: 'NSW 4147, Australia', date: '30 Sept 2014' },
];


export default class extends React.Component {
  constructor(props) {
    super();
    this.state = {
      mp: {},
      electorate: null,
      showTakingAction: false,
      updateActions: INITIAL_UPDATE_ACTIONS,
      currentActions: 1024,
      step: 0,
      mobile: this.isMobile(),
      message: 'Dear Decision Maker:',
    };

    window.addEventListener('resize', () => { 
      console.log('Window resized');
      this.setState({ mobile: this.isMobile(), tablet: this.isMobile() && window.innerWidth > 414 });
    });

    if (props.autoUpdateActions) {
      setTimeout(this.addUpdateActions.bind(this), 5000);
    }
  }

  isMobile() {
     var check = false;
     (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
    return (window.innerWidth < 964) || check;
  }

  addUpdateActions() {
    const updateActions = this.state.updateActions;

    // Clone the last update action.
    const newUpdateAction = {};
    Object.assign(newUpdateAction, updateActions[0]);

    // Update its number and key.
    newUpdateAction.number = newUpdateAction.number + 1;
    newUpdateAction.key = newUpdateAction.number;

    // Add it to the list.
    updateActions.unshift(newUpdateAction);

    // Remove the last and bump the counter.
    updateActions.pop();
    const currentActions = this.state.currentActions + 1;

    this.setState({ 
      updateActions,  
      currentActions
    });

    setTimeout(this.addUpdateActions.bind(this), 4000);
  }

  stateDropdownHandler(state) {
    console.log(this.state.electorate);
    if (state !== 'NSW') {
      this.setState({ 
        mp: null,
        showTakingAction: true 
      });
    } else if (this.state.electorate === null && !this.state.mobile) {
      this.setState({ showTakingAction: false });
    } else {
      this.electorateDropdownHandler(this.state.electorate);
    }
    this.setState({ state });
  }

  electorateDropdownHandler(electorate) {
    const mp = STATE_MPS.find(mp => mp.electorate == electorate);
    this.setState({ mp, showTakingAction: true, electorate });
  }


  renderDesktop() {
    const STICKY_ACTION_STRIP_STYLE = {position: 'fixed', top: 86, left: 0, right: 0, zIndex: 2};
    const STICKY_TAKING_ACTION_STYLE = {position: 'fixed', top: 178, left: 0, right: 0, zIndex: 2};
    const STICKY_STATUS_STYLE = {position: 'fixed', top: 178, left: 964 + ((window.outerWidth - 964) / 2) - 284, right: 0, zIndex: 2};
    const BACKDROP_STYLE = { position: 'fixed', top: 0, bottom: 0, left: 0, right: 0, backgroundColor: '#000', zIndex: -1, opacity: 0.5 };

    const { showTakingAction } = this.state;
    return (
      <div>
        <Sticky>
          <TopHeader />
        </Sticky>

        <TopBanner image="greyhound.jpg">
          Greyhound 'graveyard' = NO action from NSW industry inquiry
        </TopBanner>

        <Sticky stickyStyle={STICKY_ACTION_STRIP_STYLE} >
          <ActionStrip 
            electorate={this.state.electorate}
            state={this.state.state}
            stateHandler={this.stateDropdownHandler.bind(this)}
            electorateHandler={this.electorateDropdownHandler.bind(this)} />
        </Sticky>

        <Sticky stickyStyle={STICKY_TAKING_ACTION_STYLE} >
          <TakingAction 
            key={'taking-action'}
            mp={this.state.mp}
            visible={showTakingAction} />
        </Sticky>


        <ScrollArea>
          <MainBody/>
          <Sticky 
            forceTop={656} 
            stickyStyle={STICKY_STATUS_STYLE}
            style={{display: 'inline-block'}} onStickyStateChange={() => console.log('Sticky?')}>
            { !this.state.showTakingAction && <Status current={this.state.currentActions} max={3000} updateActions={this.state.updateActions} />  }
          </Sticky>
        </ScrollArea>

        <ReactCSSTransitionGroup transitionName="backdrop" transitionEnterTimeout={400} transitionLeaveTimeout={400} transitionAppear transitionAppearTimeout={400}>
          { showTakingAction && <div style={BACKDROP_STYLE} /> }
        </ReactCSSTransitionGroup>

        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
      </div>
    );
  }

  renderMobileActionStrip() {
    return (
      <MobileActionStrip 
        mp={this.state.mp}
        step={this.state.step}
        takeActionHandler={() => this.setState({showTakingAction : true })} 
        nextStepHandler={() => this.setState({ step: this.state.step + 1 })}
        electorate={this.state.electorate}
        state={this.state.state}
        stateHandler={this.stateDropdownHandler.bind(this)}
        electorateHandler={this.electorateDropdownHandler.bind(this)}
        max={3000} 
        key='mobile-taking-action-strip'
        current={this.state.currentActions} 
        messageHandler={(e) => this.setState({ message: event.target.value })}
        message={this.state.message}
        crossHandler={() => this.setState({ showTakingAction : false, mp: {}, electorate: null, state: null, step: 0 })}
        showTakingAction={this.state.showTakingAction}
        tablet={this.state.tablet}
      />
    );
  }


  renderMobile() {
    const showTakingAction = this.state.showTakingAction;
    const mobileAppStyle = {
      height: showTakingAction ? 90 : window.innerHeight - 110,
      overflowY: showTakingAction ? 'hidden' : 'scroll',
      overflowX: 'hidden',
      width: '100%',
      WebkitOverflowScrolling: 'touch'
    }

    console.log(this.state.step);

    return(
      <div>
        <div style={mobileAppStyle}>
          <Sticky>
            <TopHeader mobile />
          </Sticky>
          <TopBanner mobile image="greyhound.jpg">
            Greyhound 'graveyard' = NO action from NSW industry inquiry
          </TopBanner>
          <ScrollArea mobile>
            <MainBody mobile />
          </ScrollArea>
        </div>
        { this.renderMobileActionStrip() }
      </div>
    );
  }

  render() {
    return this.state.mobile ? this.renderMobile() : this.renderDesktop();
  }
};
