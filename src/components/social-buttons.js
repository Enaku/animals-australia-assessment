import React from 'react';
import ShareButton from './share-button.js';

export default function({types, mobile}) {
  return (
    <div>
      { types.map(type => <ShareButton mobile={mobile} key={type} type={type}/>) }
    </div>
  );
};
