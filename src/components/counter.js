import React from 'react';
import { TEXT_COLOUR } from '../constants/colours';
import { Motion, spring } from 'react-motion';

const numberContainerStyle = {
  display: 'inline-block',
  marginRight: '11px'
};

function currentCountStyle(mobile)  {
  return {
    fontFamily: 'Avenir',
    fontWeight: '800',
    letterSpacing: '-1.5',
    fontSize: mobile ? '2em' : '3.2em',
    marginTop: mobile ? '-5px' : '-12px',
    marginBottom: 0,
  };
}

const counterText = {
  fontSize: '0.75em',
  marginTop: 0,
  fontFamily: 'Avenir',
  marginBottom: '1px',
};

export default function({current, max, mobile}) {
  return (
    <div style={{marginTop: mobile ? 0: '59px'}}>
      <div style={numberContainerStyle}>
        {
          // Animate the count from 0 to `current`.
        }

        <Motion defaultStyle={{x: 0}} style={{x: spring(current, [300, 50])}}>
          { value => <h1 style={currentCountStyle(mobile)}>{Math.round(value.x).toLocaleString()}</h1> }
        </Motion>
      </div>
      <div style={{display: 'inline-block'}}>
        <p style={counterText}><strong>PEOPLE</strong> HAVE ACTED.</p>
        <p style={counterText}><strong style={{fontWeight: '900', color: TEXT_COLOUR}}>HELP US REACH {max.toLocaleString()}</strong></p>
      </div>
    </div>
  );
}
