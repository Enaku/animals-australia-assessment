import React from 'react';
import { CONTAINER_STYLE } from '../constants/styles';
import { ANIMALS_AUSTRALIA_ORANGE } from '../constants/colours';

const memberStyle = {
  fontFamily: 'Avenir',
  fontWeight: 300,
  fontSize: '1.5em'
};

const introTextStyle = {
  fontFamily: 'Avenir',
  fontWeight: 300,
};

const linkStyle = {
  color: ANIMALS_AUSTRALIA_ORANGE,
  textDecoration: 'none',
  fontWeight: 'bold'
}

export default function(props) {
  return (
    <div style={CONTAINER_STYLE}>
      <h1 style={memberStyle}>Sorry!</h1>
      <p style={introTextStyle}>Thank you for wanting to take action for the victims of greyhound racing.</p>
      <p style={introTextStyle}>This action is for NSW residents only, but you can make your voice heard by <a style={linkStyle} href="http://www.animalsaustralia.org/take_action/phone-your-mp-greyhound-racing/">calling your state MP.</a></p>
    </div>
  );
};
