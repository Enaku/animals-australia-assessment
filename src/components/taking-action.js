import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { CONTAINER_STYLE } from '../constants/styles';
import { TEXT_COLOUR } from '../constants/colours';
import MemberInformation from '../components/member-information';
import StepOne from '../components/step-one';
import StepTwo from '../components/step-two';
import ApologyMessage from '../components/apology-message';
import ThankyouMessage from '../components/thankyou-message';

const takingActionContainerStyle =  {
    zIndex: '1',
    paddingTop: '20px',
    paddingBottom: '80px',
    backgroundColor: '#ddd', 
    height: '550px',
    width: '100%',
    position: 'absolute'
}

const smallContainerStyle =  {
    zIndex: '1',
    paddingTop: '20px',
    paddingBottom: '80px',
    backgroundColor: '#ddd', 
    height: '150px',
    width: '100%',
    position: 'absolute'
}

const subheaderStyle = {};



export default class extends React.Component {
  constructor() {
    super();
    this.state = {
      message: 'Dear Decision Maker,',
      showTextAreaBackground:  true,
      firstName: 'Your',
      lastName: 'Name',
      showThankyou: false
    };
  }

  renderApology() {
    return (
      <div style={smallContainerStyle}>
        <ApologyMessage />
      </div>
    );
  }

  renderThankyou() {
    return (
      <div style={smallContainerStyle}>
        <div style={CONTAINER_STYLE}>
          <ThankyouMessage />
        </div>
      </div>
    );
  }

  submitLetterHandler() {
    this.setState({ showThankyou: true });
  }

  messageHandler(e) {
    this.setState({ message: e.target.value });
  }

  textAreaHandler(e) {
    this.setState({ showTextAreaBackground: false })
  }

  renderTakingAction() {
    return (
      <div key={1} style={takingActionContainerStyle}>
        <MemberInformation {...this.props.mp} />
        <div style={CONTAINER_STYLE}>
          <StepOne 
            firstName={this.state.firstName}
            lastName={this.state.lastName}
            message={this.state.message}
            showTextAreaBackground={this.state.showTextAreaBackground} 
            textAreaHandler={this.textAreaHandler.bind(this)}
            messageHandler={this.messageHandler.bind(this)}
          />
          <StepTwo 
            firstNameHandler={(e) => this.setState({ firstName: e.target.value })}
            lastNameHandler={(e) => this.setState({ lastName: e.target.value })}
            submitLetterHandler={this.submitLetterHandler.bind(this)}
          />
        </div>
      </div>
    );

  }

  render() {
    var Element, key;
    if (!this.props.visible) {
      return null;
    } else if (this.state.showThankyou) {
      Element = this.renderThankyou.bind(this);
      key = 2;
    } else if (!this.props.mp) {
      Element = this.renderApology.bind(this);
      key = 3;
    } else {
      Element = this.renderTakingAction.bind(this);
      key = 4;
    }

    return (
      <ReactCSSTransitionGroup transitionName="taking-action" transitionEnterTimeout={400} transitionLeaveTimeout={400} transitionAppear transitionAppearTimeout={400}>
        { Element() }
      </ReactCSSTransitionGroup>
    );
  }
};
