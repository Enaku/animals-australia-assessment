import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { CONTAINER_STYLE, MOBILE_CONTAINER_STYLE } from '../constants/styles';
import { TEXT_COLOR } from '../constants/colours';
import ActionAlert from './action-alert';

function bannerStyle(mobile) {
  return {
    backgroundImage: 'url(\'img/greyhound_background.jpg\')',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    height: mobile ? '150px' : '390px',
    verticalAlign: 'top',
    display: 'block',
    marginTop: 88,
    marginBottom: mobile ? '120px' : 0,
    zIndex: 2
  };
}

function headerStyle(mobile) { 
  return {
    zIndex: 2,
    color: mobile ? TEXT_COLOR : '#fff',
    marginTop: mobile ? '160px' : '22px',
    maxWidth: '455px',
    fontSize: mobile ? '2em' : '2.7em',
    fontFamily: 'Avenir, Helvetica',
    fontWeight: 900,
    letterSpacing: '-2'
  };
}

const actionAlertStyle = {
  marginTop: '90px',
  color: '#fff',
  backgroundColor: '#343838',
  height: '36px',
  width: '180px'
};

const alertIconStyle = {
  margin: '10px'
};

const alertTextStyle = {
  verticalAlign: 'top',
  display: 'inline-block',
  marginTop: '12px',
  fontSize: '13px',
  fontFamily: 'Avenir Next Regular, Helvetica',
};

// Mysterious nbsp is required. Nobody knows why.
export default function({children, mobile}) {
  return (
    <div style={bannerStyle(mobile)}>
      &nbsp; 

      { mobile ? '' : <ActionAlert /> }
      <div style={mobile ? MOBILE_CONTAINER_STYLE : CONTAINER_STYLE}>
        <ReactCSSTransitionGroup transitionName="loading" transitionEnterTimeout={400} transitionLeaveTimeout={400} transitionAppear transitionAppearTimeout={400}>
          <h1 style={headerStyle(mobile)}>{children}</h1>
        </ReactCSSTransitionGroup>
      </div>
    </div>
  );
}
