import React from 'react';

function headingStyle(mobile) {
  return {
    fontFamily: 'Avenir, Helvetica, Sans',
    fontWeight: '900px',
    letterSpacing: '-0.5',
    fontSize: mobile ? '1em' : '1.52em',
    marginTop: '-2px'
  };
}

export default function({children, mobile}) {
  return <h2 style={headingStyle(mobile)}>{children}</h2>
};
