import React from 'react';
import { CONTAINER_STYLE } from '../constants/styles';

const actionAlertStyle = {
  marginTop: '90px',
  color: '#fff',
  backgroundColor: '#343838',
  display: 'inline-block',
  height: '36px',
  width: '180px',
};

const mobileActionAlertStyle = {
  marginTop: '20px',
  verticalAlign: 'top',
  color: '#fff',
  backgroundColor: '#343838',
  display: 'inline-block',
  height: '36px',
  width: '150px',
  float: 'right',
};

const alertIconStyle = {
  margin: '10px'
};

const alertTextStyle = {
  verticalAlign: 'top',
  display: 'inline-block',
  marginTop: '12px',
  fontSize: '13px',
  fontFamily: 'Avenir Next Regular, Helvetica',
};

export default function({mobile}) {
  return (
    <div style={mobile ? {display: 'inline-block', float: 'right'} : CONTAINER_STYLE}>
      <div style={mobile ? mobileActionAlertStyle : actionAlertStyle}>
        <img style={alertIconStyle} src="img/alert_icon.png" />
        <p style={alertTextStyle}>ACTION ALERT</p>
      </div>
    </div>
  );
}
