import React from 'react';
import { ANIMALS_AUSTRALIA_LIGHT_ORANGE, ANIMALS_AUSTRALIA_LIGHT_GREY } from '../constants/colours';

var updateStyle = {
  width: '100%',
  height: '80px',
  backgroundImage: 'url(\'img/update_background.png\')',
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center',
  borderBottom: '1px solid ' + ANIMALS_AUSTRALIA_LIGHT_GREY,
  marginBottom: '48px'
};

var updateTextStyle = {
  display: 'inline-block',
  marginTop: '34px',
  color: ANIMALS_AUSTRALIA_LIGHT_ORANGE,
  fontFamily: 'Arial, Helvetica',
  letterSpacing: '-0.5',
  fontSize: '0.9em'
}

var updateLinkStyle = {
  color: ANIMALS_AUSTRALIA_LIGHT_ORANGE
};

export default function(props) {
  return (
    <div style={updateStyle}>
      <span style={updateTextStyle}><strong>UPDATE {props.date}</strong> &#x2015; <a href="#" style={updateLinkStyle}>{props.children} &raquo;</a></span>
    </div>
  );
};
