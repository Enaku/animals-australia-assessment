import React from 'react';
import { CONTAINER_STYLE, MOBILE_CONTAINER_STYLE } from '../constants/styles';

export default function({mobile, children}) {
  return (
    <div style={mobile ? MOBILE_CONTAINER_STYLE : CONTAINER_STYLE}>
      {children}
    </div>
  );
};
