import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';

ReactDOM.render(
  <App autoUpdateActions key={'main-area'} />,
  document.getElementById('app')
);
