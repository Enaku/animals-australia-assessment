import React from 'react';
import { Motion, spring}  from 'react-motion'
import UpdateAction from './update-action';

const springConfig = [300, 50];
const defaultStyle = {
  y: -30,
  opacity: 0.01
};

function getOpacity(index, length) {
  if (index === length) { // Last item.
    return spring(0.01, springConfig)
  } else if(index == 0) { // First item.
    return spring(1, springConfig);
  } else {                // All other items.
    return 1;
  }
}

function getStyle(y, opacity) {
  return {
      y: spring(y * 60, springConfig),
      opacity
  };
}


export default function({updateActions}) {
  return (
    <div>
      { updateActions.map((updateAction, i) => {
        const opacity = getOpacity(i, updateActions.length - 1);
        const style = getStyle(i, opacity);

        // Return a Motion container which will animate the UpdateAction
        return (
          <Motion 
            key={updateAction.number} 
            style={style} 
            defaultStyle={defaultStyle}
          >
            { style => <UpdateAction opacity={style.opacity} y={style.y} {...updateAction}  /> }
          </Motion>
        );
      })}
    </div>
  );
};
