import React from 'react';

export default function({type, mobile}) {
  const imageURL = `img/${type}.png`;
  const style = mobile ? {display: 'block', margin: '0 auto', marginBottom: '10px'} : {};
  return (
    <img style={style} className="social-icon" src={imageURL} />
  );
};
