import React from 'react';
import { CONTAINER_STYLE } from '../constants/styles';
import { ANIMALS_AUSTRALIA_ORANGE } from '../constants/colours';

const memberStyle = {
  fontFamily: 'Avenir',
  fontWeight: 300,
  fontSize: '1.5em'
};

const introTextStyle = {
  fontFamily: 'Avenir',
  fontWeight: 300,
};

const linkStyle = {
  color: ANIMALS_AUSTRALIA_ORANGE,
  textDecoration: 'none',
  fontWeight: 'bold'
}

export default function(props) {
  return (
    <div style={CONTAINER_STYLE}>
      <h1 style={memberStyle}>Thank you</h1>
      <p style={introTextStyle}>Thanks for taking action to help the victims of Greyhound Racing!</p>
    </div>
  );
};
