import React from 'react';

var donateCTAStyle = {
  height: '43px',
  width: '311px',
  margin: '0 auto',
  color: '#fff',
  letterSpacing: '-0.5',
  fontSize: '1.5em',
  paddingTop: '12px',
  textAlign: 'center',
  fontFamily: 'Avenir Next, Helvetica, Sans',
  background: 'url(\'img/donate_button_background.png\') no-repeat',
  zIndex: '-500',
  cursor: 'pointer' // TODO: Not working?
};

//TODO Do something here
function logClick() {
  window.location = 'https://secure.animalsaustralia.org/donate/';
}

export default function() {
  return <div onClick={logClick} style={donateCTAStyle}>DONATE &raquo;</div>;
};
