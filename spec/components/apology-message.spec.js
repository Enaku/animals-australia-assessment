import ApologyMessage from '../../src/components/apology-message.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);
describe('ApologyMessage', () => {
  function render(props) {
    const renderer = TestUtils.createRenderer();
    renderer.render(<ApologyMessage {...props} />);
    return renderer.getRenderOutput();
  }

  it('should render an apology message', () => {
    const actual = render({visible: false});
    expect(actual.props.children[0].type).toBe('h1');
  });
});
