import ActionAlert from '../../src/components/action-alert';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('ActionAlert', () => {
  it('should render ACTION ALERT', () => {
    const renderer = TestUtils.createRenderer();
    renderer.render(<ActionAlert />);
    const actual = renderer.getRenderOutput();
    const expected = 'ACTION ALERT';
    expect(actual).toIncludeJSX(expected);
  });
});
