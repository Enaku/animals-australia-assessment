import TopBanner from '../../src/components/top-banner.js';
import ActionAlert from '../../src/components/action-alert';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('TopBanner', () => {
  it('should render some header text', () => {
    const renderer = TestUtils.createRenderer();
    const testText = "Hello audrey";
    renderer.render(<TopBanner>{testText}</TopBanner>);
    const actual = renderer.getRenderOutput();
    const expected = testText;
    expect(actual).toIncludeJSX(expected);
  });

  it('should not render the action alert on mobile', () => {
    const renderer = TestUtils.createRenderer();
    const testText = "Hello audrey";
    renderer.render(<TopBanner mobile>{testText}</TopBanner>);
    const actual = renderer.getRenderOutput();
    const expected = <ActionAlert />;
    expect(actual).toNotIncludeJSX(expected);
  });
});
