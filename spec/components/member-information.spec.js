import MemberInformation from '../../src/components/member-information';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

const headingStyle = {
  marginTop: '59px',
  fontFamily: 'Avenir, Helvetica, Sans',
  fontWeight: '900px',
  letterSpacing: '-0.5'
};
describe('MemberInformation', () => {
  it('should render information about your MP', () => {
    const renderer = TestUtils.createRenderer();
    const MP = { first_name: 'Guy', surname: 'Zangori', electorate: 'Fairfield', party: 'Labor' };
    renderer.render(<MemberInformation {...MP} />);
    const actual = renderer.getRenderOutput();
    Object.keys(MP).forEach(k => expect(actual).toIncludeJSX(MP[k]));
  });
});
