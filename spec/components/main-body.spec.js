import MainBody from '../../src/components/main-body.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('MainBody', () => {
  it('should render the main body with its children', () => {
    const renderer = TestUtils.createRenderer();
    const expected = <div/>
    renderer.render(<MainBody/>);
    const actual = renderer.getRenderOutput();
    expect(actual.props.children.length).toBe(11);
  });

  it('should set its width to 100% on mobile', () => {
    const renderer = TestUtils.createRenderer();
    const expected = <div/>
    renderer.render(<MainBody mobile/>);
    const actual = renderer.getRenderOutput();
    expect(actual.props.style.width).toBe('100%');
  });
});
