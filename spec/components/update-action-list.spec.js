import UpdateActionList from '../../src/components/update-action-list.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('UpdateActionList', () => {
  it('should render a list of UpdateActions', () => {
    const renderer = TestUtils.createRenderer();
    const updateActions = [
      { number: 2863, name: 'Maggie Stymans', location: 'NSW 4147, Australia', date: '30 September 2014' },
      { number: 2864, name: 'Maggie Stymans', location: 'NSW 4147, Australia', date: '30 September 2014' },
      { number: 2865, name: 'Maggie Stymans', location: 'NSW 4147, Australia', date: '30 September 2014' },
      { number: 2866, name: 'Maggie Stymans', location: 'NSW 4147, Australia', date: '30 September 2014' },
      { number: 2867, name: 'Maggie Stymans', location: 'NSW 4147, Australia', date: '30 September 2014' },
    ];
    renderer.render(<UpdateActionList updateActions={updateActions} />);
    const actual = renderer.getRenderOutput();
    expect(actual.props.children.length).toEqualJSX(updateActions.length);
  });
});
