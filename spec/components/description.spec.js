import Description from '../../src/components/description.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('Description', () => {
  it('should render some introduction text', () => {
    const renderer = TestUtils.createRenderer();
    const expectedText = 'testing'; 
    renderer.render(<Description>{expectedText}</Description>);
    const actual = renderer.getRenderOutput();
    expect(actual).toIncludeJSX(expectedText);
    expect(actual.type).toBe('p');
  });
});
