import DonateHeader from '../../src/components/donate-header.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('DonateHeader', () => {
  it('should render a header', () => {
    const renderer = TestUtils.createRenderer();
    const expected = 'DONATE TO SUPPORT ANIMALS AUSTRALIA’S WORK';
    renderer.render(<DonateHeader/>);
    const actual = renderer.getRenderOutput();
    expect(actual).toIncludeJSX(expected);
    expect(actual.type).toBe('h3');
  });
});
