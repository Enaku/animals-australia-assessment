import StepOne from '../../src/components/step-one.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

const headingStyle = {
  marginTop: '59px',
  fontFamily: 'Avenir, Helvetica, Sans',
  fontWeight: '900px',
  letterSpacing: '-0.5'
};
describe('StepOne', () => {
  function render(props) {
    const renderer = TestUtils.createRenderer();
    renderer.render(<StepOne {...props} />);
    return renderer.getRenderOutput();
  }

  it('should render a text area with the correct message', () => {
    const message = 'testing';
    const actual = render({ message });
    expect(actual).toIncludeJSX(message);
  });

  it('should not render a background on the textarea if showTextAreaBackground is false', () => {
    const showTextAreaBackground = false;
    const actual = render({ showTextAreaBackground });
    const textArea = actual.props.children.filter(c => c.type == 'textarea')[0];
    expect(textArea.props.style.backgroundImage).toNotExist();
    //expect(actual).toIncludeJSX(message);
  });
});
