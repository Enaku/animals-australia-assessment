import ThankyouMessage from '../../src/components/thankyou-message.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);
describe('ThankyouMessage', () => {
  function render(props) {
    const renderer = TestUtils.createRenderer();
    renderer.render(<ThankyouMessage {...props} />);
    return renderer.getRenderOutput();
  }

  it('should render thank you message', () => {
    const actual = render({visible: false});
    expect(actual.props.children[0].type).toBe('h1');
    expect(actual).toIncludeJSX('Thank you');
  });
});
