import Counter from '../../src/components/counter.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('Counter', () => {
  it('should render a counter with the max count', () => {
    const renderer = TestUtils.createRenderer();
    const max = 2000;
    const current = 1000;
    renderer.render(<Counter max={max} current={current} />);

    const actual = renderer.getRenderOutput();
    expect(actual).toIncludeJSX('2,000');
  });
});
