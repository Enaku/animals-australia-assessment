import Update from '../../src/components/update.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('Update', () => {
  function renderUpdate(date, text) {
    const renderer = TestUtils.createRenderer();
    renderer.render( <Update date={date}>{text}</Update>);

    return renderer.getRenderOutput();
  }

  it('should render the correct date', () => {
    const expectedDate = "2015-01-01";
    const actual = renderUpdate(expectedDate, "");
    expect(actual).toIncludeJSX(expectedDate);
  });
  
  it('should render the correct text', () => {
    const expectedUpdateText = "Something has happened!";
    const actual = renderUpdate("", expectedUpdateText);
    expect(actual).toIncludeJSX(expectedUpdateText);
  });

  it('should contain the word UPDATE', () => {
    const actual = renderUpdate("", "");
    expect(actual).toIncludeJSX("UPDATE");
  });
});
