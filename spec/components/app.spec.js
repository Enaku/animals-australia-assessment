import App from '../../src/components/app.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
import jsdom from 'mocha-jsdom';
expect.extend(expectJSX);

describe('App', () => {
  jsdom();

  it('should render the app', (done) => {
    const renderer = TestUtils.createRenderer();
    renderer.render(<App/>);
    const actual = renderer.getRenderOutput();
    expect(actual.props.children.length > 1).toBeTruthy();
    done();
  });
});
