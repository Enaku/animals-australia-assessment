import MobileActionStrip from '../../src/components/mobile-action-strip';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
import jsdom from 'mocha-jsdom';
expect.extend(expectJSX);

describe('MobileActionStrip', () => {
  jsdom();
  it('should render the MobileActionStrip', () => {
    const renderer = TestUtils.createRenderer();
    renderer.render(<MobileActionStrip/>);
    const actual = renderer.getRenderOutput();
    const expected = 'ReactCSSTransitionGroup';
    expect(actual).toIncludeJSX(expected);
    expect(actual.props.children).toBeTruthy();
  });
});
