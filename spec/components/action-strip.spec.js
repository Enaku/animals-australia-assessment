import ActionStrip from '../../src/components/action-strip.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
import jsdom from 'mocha-jsdom';
expect.extend(expectJSX);

describe('ActionStrip', () => {
  jsdom();

  function render() {
    const renderer = TestUtils.createRenderer();
    renderer.render(<ActionStrip/>);
    return renderer.getRenderOutput();
  }

  // TODO: Do a full render to get to contents?
  it('should render a <div> container', () => {
    var actual = render();
    var exected = <h3>TAKE ACTION:</h3>
    expect(actual.type).toBe('div');
  });
  
});
