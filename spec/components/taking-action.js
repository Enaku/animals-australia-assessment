import TakingAction from '../../src/components/taking-action.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);
describe('TakingAction', () => {
  function render(props) {
    const renderer = TestUtils.createRenderer();
    renderer.render(<TakingAction {...props} />);
    return renderer.getRenderOutput();
  }

  it('should should not render when visible is false', () => {
    const actual = render({visible: false});
    expect(actual).toNotExist();
  });

  it('should render an apology when there is no MP', () => {
    const actual = render({visible: true});
    expect(actual.props.children.props.style.height).toNotBe('550px');
  });
});
