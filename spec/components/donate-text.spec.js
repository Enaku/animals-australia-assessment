import DonateText from '../../src/components/donate-text.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('DonateText', () => {
  it('should render a plea to donate', () => {
    const renderer = TestUtils.createRenderer();
    const expected = 'Your financial support is needed to support Animals Australia’s ongoing work to protect the most vulnerable and abused animals in our society. Our campaigns are funded entirely by public donations. Please give generously to help put an end to animal cruelty.';
    renderer.render(<DonateText/>);
    const actual = renderer.getRenderOutput();
    expect(actual).toIncludeJSX(expected);
  });
});
