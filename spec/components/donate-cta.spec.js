import DonateCTA from '../../src/components/donate-cta.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('DonateCTA', () => {
  it('should render a big DONATE button', () => {
    const renderer = TestUtils.createRenderer();
    const expected = 'DONATE »';
    renderer.render(<DonateCTA/>);
    const actual = renderer.getRenderOutput();
    expect(actual).toIncludeJSX(expected);
    expect(actual.type).toBe('div');
  });
});
