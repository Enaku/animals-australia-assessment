import SocialButtons from '../../src/components/social-buttons.js';
import ShareButton from '../../src/components/share-button.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('SocialButtons', () => {
  it('should render two social buttons', () => {
    const renderer = TestUtils.createRenderer();
    renderer.render(<SocialButtons mobile types={['twitter', 'facebook']} />);
    const actual = renderer.getRenderOutput();
    expect(actual.props.children.length).toBe(2);
    expect(actual).toIncludeJSX(<ShareButton mobile={true} type='twitter' />);
    expect(actual).toIncludeJSX(<ShareButton mobile={true} type='facebook' />);
  });
});
