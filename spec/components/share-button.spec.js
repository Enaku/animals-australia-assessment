import ShareButton from '../../src/components/share-button.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('SocialButtons', () => {
  it('should render a share button', () => {
    const renderer = TestUtils.createRenderer();
    const testButtonType = 'twitter';
    renderer.render(<ShareButton type={testButtonType} />);
    const actual = renderer.getRenderOutput();
    expect(actual.type).toBe('img');
    expect(actual.props.src).toBe('img/twitter.png');
  });
});
