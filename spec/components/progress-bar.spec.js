import ProgressBar from '../../src/components/progress-bar.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('ProgressBar', () => {
  it('should render a progress bar in a Motion container', () => {
    const renderer = TestUtils.createRenderer();
    renderer.render(<ProgressBar max={1} current={5} />);

    const actual = renderer.getRenderOutput();
    expect(actual.props.children.type.displayName).toBe('Motion');
  });
});
