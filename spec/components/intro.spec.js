import Intro from '../../src/components/intro.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('Intro', () => {
  it('should render some introduction text', () => {
    const renderer = TestUtils.createRenderer();
    const expectedText = 'testing'; 
    renderer.render(<Intro>{expectedText}</Intro>);
    const actual = renderer.getRenderOutput();
    expect(actual).toIncludeJSX(expectedText);
    expect(actual.type).toBe('h2');
  });

  it('should reduce the font size on mobile', () => {
    const renderer = TestUtils.createRenderer();
    const expectedText = 'testing'; 
    renderer.render(<Intro mobile>{expectedText}</Intro>);
    const actual = renderer.getRenderOutput();
    expect(actual.props.style.fontSize).toBe('1em');
  });
});
