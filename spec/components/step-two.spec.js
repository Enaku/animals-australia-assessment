import StepTwo from '../../src/components/step-two.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('StepTwo', () => {
  it('should render step two', () => {
    const renderer = TestUtils.createRenderer();
    renderer.render(<StepTwo />);
    const actual = renderer.getRenderOutput();
    expect(actual.type).toEqual('div');
  });
});
