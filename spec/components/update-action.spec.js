import UpdateAction from '../../src/components/update-action.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('UpdateAction', () => {
  it('should render an UpdateAction', () => {
    const renderer = TestUtils.createRenderer();
    const updateAction = { number: 2863, name: 'Maggie Stymans', location: 'NSW 4147, Australia', date: '30 September 2014' };
    renderer.render(<UpdateAction {...updateAction} y={5} />);
    const actual = renderer.getRenderOutput();

    const expectedNumber = '#2,863';
    expect(actual).toIncludeJSX(expectedNumber);

    const expectedName = updateAction.name + ',';
    expect(actual).toIncludeJSX(expectedName);

    const expectedLocation = updateAction.location;
    expect(actual).toIncludeJSX(expectedLocation);

    const expectedDate = updateAction.date;
    expect(actual).toIncludeJSX(expectedDate);
  });
});
