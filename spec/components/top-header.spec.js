import TopHeader from '../../src/components/top-header.js';
import ActionAlert from '../../src/components/action-alert';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('TopHeader', () => {
  it('should have the AnimalsAustralia logo', () => {
    const renderer = TestUtils.createRenderer();
    renderer.render(<TopHeader/>);
    const actual = renderer.getRenderOutput();
    const expected = "img/animals_australia_logo.png";
    expect(actual).toIncludeJSX(expected);
    expect(actual.props.children.type).toBe('div');
  });

  it('should have the ACTION ALERT text when in mobile', () => {
    const renderer = TestUtils.createRenderer();
    renderer.render(<TopHeader mobile />);
    const actual = renderer.getRenderOutput();
    const expected = <ActionAlert mobile />;
    expect(actual).toIncludeJSX(expected);
  });
});
