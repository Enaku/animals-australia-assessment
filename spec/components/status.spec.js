import Status from '../../src/components/status.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('Status', () => {
  it('should render the counter, progress bar and a list of action records', () => {
    const renderer = TestUtils.createRenderer();
    renderer.render(<Status />);
    const actual = renderer.getRenderOutput();
    expect(actual.props.children.length).toEqualJSX(3);
  });
});
