import ScrollArea from '../../src/components/scroll-area.js';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

describe('ScrollArea', () => {
  it('should render a scroll area with its children inside', () => {
    const renderer = TestUtils.createRenderer();
    const expected = <img src="hello" />;
    renderer.render(<ScrollArea>{expected}</ScrollArea>);
    const actual = renderer.getRenderOutput();
    expect(actual).toIncludeJSX(expected);
    expect(actual.type).toBe('div');
  });
});
